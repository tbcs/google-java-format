# Docker image of google-java-format

Docker packaged version of [google-java-format](https://github.com/google/google-java-format) meant for use with [pre-commit](http://pre-commit.com/).

## Configuration for pre-commit

`.pre-commit-config.yaml`:

```yaml
repos:
  - repo: https://gitlab.com/tbcs/google-java-format
    rev: v1.7
    hooks:
      - id: google-java-format
```
