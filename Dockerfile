FROM openjdk:11-jre-slim
ADD https://github.com/google/google-java-format/releases/download/google-java-format-1.7/google-java-format-1.7-all-deps.jar /usr/local/lib/google-java-format.jar
RUN chmod 644 /usr/local/lib/google-java-format.jar
ENTRYPOINT ["/usr/local/openjdk-11/bin/java", "-jar", "/usr/local/lib/google-java-format.jar"]
